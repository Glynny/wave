package wave

import "fmt"
import "math"

const PI2 = 2 * math.Pi

type squareWave struct {
	DutyCycle float64
}

type argError struct {
	arg  float64
	prob string
}

func SquareWave(dutyCycle float64) (*squareWave, error) {
	sW := new(squareWave)
	switch {
	case dutyCycle < 0:
		return sW, &argError{dutyCycle, "Negative dutyCycle given."}
	case dutyCycle > 1.0:
		return sW, &argError{dutyCycle, "dutyCycle greater than 1 given."}
	}
	sW.DutyCycle = dutyCycle
	return sW, nil
}

func (sw squareWave) Value(phase float64) float64 {
	phase = math.Mod(phase, PI2)
	switch {
	case phase >= 0.0 && phase < PI2*sw.DutyCycle:
		return 1.0
	}
	return 0.0
}

func (e *argError) Error() string {
	return fmt.Sprintf("%d - %s", e.arg, e.prob)
}
