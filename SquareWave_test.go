package wave

import "testing"
import "math"

type PhaseValueTest struct {
	phase         float64
	expectedValue float64
}

type SquareWaveTest struct {
	sW           squareWave
	resultsTests []PhaseValueTest
}

var testValues = []SquareWaveTest{
	{
		squareWave{0.0}, []PhaseValueTest{
			{0.0, 0.0},
			{math.Pi - 0.00000000001, 0.0},
			{math.Pi, 0.0},
			{math.Pi + 0.1, 0.0},
			{2 * math.Pi, 0.0},
		},
	},
	{
		squareWave{0.5}, []PhaseValueTest{
			{0.0, 1.0},
			{math.Pi - 0.00000000001, 1.0},
			{math.Pi, 0.0},
			{math.Pi + 0.1, 0.0},
			{2 * math.Pi, 1.0},
		},
	},
	{
		squareWave{1.0}, []PhaseValueTest{
			{0.0, 1.0},
			{math.Pi - 0.00000000001, 1.0},
			{math.Pi, 1.0},
			{math.Pi + 0.1, 1.0},
			{2 * math.Pi, 1.0},
		},
	},
}

func TestCreateSquareWave(t *testing.T) {
	_, _ = SquareWave(0.5)
}

func TestFailOnNegativeDutyCycle(t *testing.T) {
	sW, err := SquareWave(-0.00001)
	if err == nil {
		t.Error("Expected error for negative duty cycle but not returned.", sW)
	}
}

func TestFailOnDutyCycleGreaterThan1(t *testing.T) {
	sW, err := SquareWave(1.0000000000001)
	if err == nil {
		t.Error("Expected error for duty cycle greater than 1 but no error returned.", sW)
	}
}

func TestKnownResults(t *testing.T) {
	for waveIndex, testValue := range testValues {
		squareWave := testValue.sW
		for resultsIndex, resultsTest := range testValue.resultsTests {
			expectedValue := resultsTest.expectedValue
			actualResult := squareWave.Value(resultsTest.phase)
			if actualResult != resultsTest.expectedValue {
				t.Errorf("Test for known result.\nWaveIndex:%d\nResultsIndex:%d\nExpected:%f\nReturned:%f\nWave:%+v\nPhase: %f\nDutyPhase: %f\nphaseSmaller:%t)",
					waveIndex,
					resultsIndex,
					expectedValue,
					actualResult,
					squareWave,
					resultsTest.phase,
					PI2*squareWave.DutyCycle,
					resultsTest.phase < PI2*squareWave.DutyCycle,
				)
			}
		}
	}
}
